package com.dao.factory;

import com.bean.entity.*;
import com.dao.Impl.BaseDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoFactory {
    @Autowired
    private BaseDaoImpl<Author> authorDao;
    @Autowired
    private BaseDaoImpl<Book> bookDao;
    @Autowired
    private BaseDaoImpl<Genre> genreDao;
    @Autowired
    private BaseDaoImpl<User> userDao;
    @Autowired
    private BaseDaoImpl<Role> roleDao;

    public BaseDaoImpl<Author> getAuthorDao() {
        return authorDao;
    }

    public BaseDaoImpl<Book> getBookDao() {
        return bookDao;
    }

    public BaseDaoImpl<Genre> getGenreDao() {
        return genreDao;
    }

    public BaseDaoImpl<User> getUserDao() {
        return userDao;
    }

    public BaseDaoImpl<Role> getRoleDao() {
        return roleDao;
    }
}