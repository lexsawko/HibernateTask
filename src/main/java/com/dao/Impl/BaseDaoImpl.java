package com.dao.Impl;

import com.dao.IBaseDao;
import com.dao.exception.DaoException;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;


public class BaseDaoImpl<T> implements IBaseDao<T> {
    private SessionFactory sessionFactory;
    private Transaction transaction = null;
    private Class<T> clazz;
    private static Logger logger = Logger.getLogger(BaseDaoImpl.class);

    public BaseDaoImpl(SessionFactory sessionFactory, Class<T> clazz){
        this.sessionFactory = sessionFactory;
        this.clazz = clazz;
    }

    @Override
    public List<T> findAll() throws DaoException {
        String HQL_GET_ALL = "From " + clazz.getSimpleName();
        List<T> list = null;
        try {
            Session session1 = sessionFactory.openSession();
            list = session1.createQuery(HQL_GET_ALL).list();
            session1.close();
        } catch (Throwable e){
            logger.debug("Find all error", e);
            throw new DaoException("Find all error");
        }
        return list;
    }

    @Override
    public T findEntityById(Integer id) throws DaoException{
        Session session1 = sessionFactory.openSession();
         T obj = session1.get(clazz,id);
         session1.close();
        return obj;
    }

    @Override
    public void delete(T entity) throws DaoException{
        try {
            Session session1 = sessionFactory.openSession();
            transaction = session1.beginTransaction();
            session1.delete(entity);
            session1.flush();
            transaction.commit();
            session1.close();
        } catch (Throwable e) {
            logger.debug("Delete error", e);
            transaction.rollback();
            throw new DaoException("Delete error - entity " + entity.getClass().getSimpleName());
        }
    }

    @Override
    public void create(T entity) throws DaoException{
        try{
            Session session1 = sessionFactory.openSession();
        transaction = session1.beginTransaction();
            session1.save(entity);
            session1.flush();
        transaction.commit();
        session1.close();
        } catch (Throwable e){
            logger.debug("Create error", e);
            transaction.rollback();
            throw new DaoException("Create error");
        }
    }

    @Override
    public void update(T entity) throws DaoException{
        try{
            Session session1 = sessionFactory.openSession();
        transaction = session1.beginTransaction();
            session1.update(entity);
            session1.flush();
        transaction.commit();
        session1.close();
        }catch (Throwable e){
            logger.debug("Update error", e);
            transaction.rollback();
            throw new DaoException("Update error");
        }
    }

    @Override
    public Integer getNumberOfRows() throws DaoException{
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        String HQL_GET_NUMBER_OF_ROWS = "select count(*) From " + clazz.getSimpleName();
        Integer count = (Integer) session.createQuery(HQL_GET_NUMBER_OF_ROWS).getResultList().get(0);
        session.getTransaction().commit();
        session.close();
        return count;
    }
}
