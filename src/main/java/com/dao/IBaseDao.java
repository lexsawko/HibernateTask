package com.dao;

import com.dao.exception.DaoException;

import java.util.List;

public interface IBaseDao<T> {
    List<T> findAll() throws DaoException;
    T findEntityById(Integer id) throws DaoException;
    void delete(T entity) throws DaoException;
    void create(T entity) throws DaoException;
    void update(T entity) throws DaoException;
    Integer getNumberOfRows() throws DaoException;
}
