package com.util.converter;

import com.bean.entity.Role;
import com.dao.exception.DaoException;
import com.dao.factory.DaoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import org.springframework.stereotype.Component;

@Component
public class StringToRole implements Converter<String,Role> {
    @Autowired
    private DaoFactory daoFactory;

    @Override
    public Role convert(String s) {
        Role role = null;
        try {
            role = daoFactory.getRoleDao().findEntityById(Integer.parseInt(s));
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return role;
    }
}
