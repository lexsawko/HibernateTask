package com.util.converter;

import com.bean.entity.Author;
import com.dao.exception.DaoException;
import com.dao.factory.DaoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToAuthor implements Converter<String,Author> {
    @Autowired
    DaoFactory daoFactory;

    @Override
    public Author convert(String s) {
        Author author = null;
        try{
            author = daoFactory.getAuthorDao().findEntityById(Integer.parseInt(s));
        } catch (DaoException e){

        }
        return author;
    }
}
