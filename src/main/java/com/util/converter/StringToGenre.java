package com.util.converter;

import com.bean.entity.Genre;
import com.dao.exception.DaoException;
import com.dao.factory.DaoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToGenre implements Converter<String,Genre> {
    @Autowired
    private DaoFactory daoFactory;

    @Override
    public Genre convert(String s) {
        Genre genre = null;
        try {
            genre = daoFactory.getGenreDao().findEntityById(Integer.parseInt(s));
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return genre;
    }
}
