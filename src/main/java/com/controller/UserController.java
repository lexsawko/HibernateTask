package com.controller;

import com.bean.entity.User;
import com.dao.exception.DaoException;
import com.dao.factory.DaoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {
    @Autowired
    DaoFactory daoFactory;

    @GetMapping("/users")
    public String getUsers(Model model){
        try {
            model.addAttribute("users", daoFactory.getUserDao().findAll());
        }catch (DaoException e){

        }
        return "userList";
    }

    @GetMapping("/edit_user")
    public String editUser(@RequestParam(value = "id", required = false) Integer id,
                           Model model){
        try {
            if(id!=null){
                model.addAttribute("method","Edit");
                    model.addAttribute("user", daoFactory.getUserDao()
                            .findEntityById(id));

            } else {
                model.addAttribute("user", new User());
                model.addAttribute("method","Add");
            }
        } catch (DaoException e) {

        }
        return "editUser";
    }


    @RequestMapping("/update_user")
    public String addUser(@Valid User user, BindingResult result){
        if(result.hasErrors()){
            return "/editUser";
        }
        if(user.getId() == null){
            try {
                daoFactory.getUserDao().create(user);
            } catch (DaoException e) {
                e.printStackTrace();
            }
        } else {
            try {
                daoFactory.getUserDao().update(user);
            } catch (DaoException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/users";
    }

    @RequestMapping("/delete_user")
    public String addUser(@RequestParam("id") Integer id){
        User user;
        try {
            user = daoFactory.getUserDao().findEntityById(id);
            daoFactory.getUserDao().delete(user);
        } catch (DaoException e) {
            e.printStackTrace();
        }
        return "redirect:/users";
    }
}
