package com.controller;

import com.bean.entity.Author;
import com.dao.exception.DaoException;
import com.dao.factory.DaoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AuthorController {
    @Autowired
    DaoFactory daoFactory;

    @GetMapping("/edit_author")
    public String editAuthor(@RequestParam(value = "id", required = false) Integer id, Model model){
        if(id!=null){
            model.addAttribute("method","Edit");
            try {
                model.addAttribute("author", daoFactory.getAuthorDao()
                        .findEntityById(id));
            } catch (DaoException e) {

            }
        } else {
            model.addAttribute("author", new Author());
            model.addAttribute("method","Add");
        }
        return "/editAuthor";
    }

    @RequestMapping("/delete_author")
    public String deleteAuthor(@RequestParam("id") Integer id){
        Author author = null;
        try {
            author =  daoFactory.getAuthorDao().findEntityById(id);
            daoFactory.getAuthorDao().delete(author);
        } catch (DaoException e) {

        }
        return "redirect:/library";
    }

    @PostMapping("/update_author")
    public String updateAuthor(@ModelAttribute("author") Author author){
        if(author.getId()==null){
            try {
                daoFactory.getAuthorDao().create(author);
            } catch (DaoException e) {

            }
        } else {
            try {
                daoFactory.getAuthorDao().update(author);
            } catch (DaoException e) {

            }
        }
        return "redirect:/library";
    }
}
