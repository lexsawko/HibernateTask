package com.controller;

import com.bean.entity.Genre;
import com.dao.exception.DaoException;
import com.dao.factory.DaoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Controller
public class GenreController {
    @Autowired
    DaoFactory daoFactory;

    @GetMapping("/edit_genre")
    public String editGenre(@RequestParam(value = "id", required = false) Integer id, Model model){
        if(id!=null){
            model.addAttribute("method","Edit");
            try {
                model.addAttribute("genre", daoFactory.getGenreDao()
                        .findEntityById(id));
            } catch (DaoException e) {

            }
        } else {
            model.addAttribute("genre", new Genre());
            model.addAttribute("method","Add");
        }
        return "editGenre";
    }

    @RequestMapping("/delete_genre")
    public String deleteGenre(@RequestParam("id") Integer id){
        Genre genre = null;
        try {
            genre = daoFactory.getGenreDao().findEntityById(id);
            daoFactory.getGenreDao().delete(genre);
        } catch (DaoException e) {
            try {
               FileWriter fileWriter =  new FileWriter(new File("C:\\Users\\User\\IdeaProjects\\HibernateTask\\src\\main\\resources\\error.txt"),false);
               fileWriter.write("Deleting error -  genre id = " + id + ", name = " + genre.getName());
               fileWriter.flush();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return "redirect:/library";
    }

    @PostMapping("/update_genre")
    public String updateGenre(@ModelAttribute("genre") Genre genre){
        if(genre.getId() == null){
            try {
                daoFactory.getGenreDao().create(genre);
            } catch (DaoException e) {

            }
        } else {
            try {
                daoFactory.getGenreDao().update(genre);
            } catch (DaoException e) {

            }
        }
        return "redirect:/library";
    }
}
