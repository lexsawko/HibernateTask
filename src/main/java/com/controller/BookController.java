package com.controller;

import com.bean.entity.Book;
import com.dao.exception.DaoException;
import com.dao.factory.DaoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Controller
public class BookController {
    @Autowired
    DaoFactory daoFactory;

    @GetMapping("/edit_book")
    public String editBook(@RequestParam(value = "id", required = false) Integer id, Model model){
        try {
//            model.addAttribute("genres", daoFactory.getGenreDao().findAll());
//            model.addAttribute("authors",daoFactory.getAuthorDao().findAll());
            if(id!=null){
                model.addAttribute("method","Edit");
                model.addAttribute("book", daoFactory.getBookDao()
                        .findEntityById(id));
            } else {
                model.addAttribute("book", new Book());
                model.addAttribute("method", "Add");
            }
        } catch (DaoException e) {
        }
        return "editBook";
    }

    @RequestMapping("/delete_book")
    public String deleteBook(@RequestParam("id") Integer id){
        Book book = null;
        try {
            book = daoFactory.getBookDao().findEntityById(id);
            daoFactory.getBookDao().delete(book);
        } catch (DaoException e) {

        }
        return "redirect:/library";
    }

    @RequestMapping("/update_book")
    public String updateBook(@Valid Book book, BindingResult result){
        if(result.hasErrors()){
            return "/editBook";
        }
        if(book.getId() == null){
            try {
                daoFactory.getBookDao().create(book);
            } catch (DaoException e){

            }
        } else {
            try {
                daoFactory.getBookDao().update(book);
            } catch (DaoException e) {

            }
        }
        return "redirect:/library";
    }
}
