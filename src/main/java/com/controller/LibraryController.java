package com.controller;

import com.bean.entity.Author;
import com.bean.entity.Book;
import com.bean.entity.Genre;
import com.dao.exception.DaoException;
import com.dao.factory.DaoFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class LibraryController {
    @Autowired
    DaoFactory daoFactory;

    @RequestMapping("/library")
    public String getLibrary(Model model){
        List<Book> books;
        List<Author> authors;
        List<Genre> genres;
        try {
            books = daoFactory.getBookDao().findAll();
            model.addAttribute("books",books);
            authors = daoFactory.getAuthorDao().findAll();
            model.addAttribute("authors", authors);
            genres = daoFactory.getGenreDao().findAll();
            model.addAttribute("genres", genres);
        } catch (DaoException e) {

        }
        return "library";
    }
}
