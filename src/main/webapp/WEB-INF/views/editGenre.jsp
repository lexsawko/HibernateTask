<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <title>${method} genre</title>
</head>
<body>
<h1>${method} genre</h1>
<form:form action="update_genre" modelAttribute="genre">
    <table class="margin">
        <c:if test="${method eq 'Edit'}">
            <th>ID</th>
            <td><form:input type="text" path="id" placeholder="ID" readonly="true"/></td>
        </c:if>
        <tr>
            <th>Name</th>
            <td><form:input type="text" path="name" placeholder="Name" required="true"/></td>
        </tr>
    </table>
    <button type="submit" class="btn-save">Save</button>
    <a href="/library"><button type="button" style="margin-left: 20px">Back to library</button></a>
</form:form>
</body>
</html>
