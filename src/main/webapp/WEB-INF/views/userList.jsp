<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>

<html>
<head>
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <title>User list</title>
</head>
<body class="margin">
<h2>Users</h2>
<a href="edit_user"><button type="submit" class="btn-add">Add user</button></a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Password</th>
        <th>Email</th>
        <th>Role</th>
    </tr>
    <c:forEach var="row" items="${users}">
        <tr>
            <td>
                    ${row.id}
            </td>
            <td>
                    ${row.name}
            </td>
            <td>
                    ${row.password}
            </td>
            <td>
                    ${row.email}
            </td>
            <td>
                    ${row.role.role}
            </td>
            <td>
                <a href="edit_user?id=${row.id}">
                    <button type="button" class="btn-edit">Edit</button></a>
                <form method="post" action="delete_user?id=${row.id}">
                    <button type="submit" class="btn-delete">Delete</button>
                </form>
            </td>
        </tr>
    </c:forEach>
    <a href="/library"><button type="button" style="margin-left: 20px">Back to library</button></a>
</table>
</body>
</html>
