<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>

<sql:query var="roles" dataSource="jdbc/TestDB">
select * from role
</sql:query>

<html>
<head>
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <title>${method} user</title>
</head>
<body>
<h1>${method} user</h1>
        <form:form action="update_user" modelAttribute="user">
            <table class="margin">
                <c:if test="${method eq 'Edit'}">
                    <th>ID</th>
                    <td><form:input type="number" path="id" placeholder="ID" readonly="true"/></td>
                    <td><form:errors path="id"/></td>
                </c:if>
                <tr>
                    <th>Name</th>
                    <td><form:input type="text" path="name" placeholder="Name" value=""/></td>
                    <td><form:errors path="name"/></td>
                </tr>
                <tr>
                    <th>Password</th>
                    <td><form:input type="text" path="password" placeholder="Password" value=""/></td>
                    <td><form:errors path="password"/></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><form:input type="email" path="email" placeholder="Email" value=""/></td>
                    <td><form:errors path="email"/></td>
                </tr>
                <tr>
                    <th>Role</th>
                    <td>
                        <form:select path="role">
                            <c:forEach var="Role" items="${roles.rows}">
                                <form:option value="${Role.id}">${Role.role}</form:option>
                            </c:forEach>
                        </form:select>
                        <td><form:errors path="role"/></td>
                    </td>
                </tr>
            </table>
            <button type="submit" class="btn-save">Save</button>
            <a href="/users"><button type="button" style="margin-left: 20px">Back to user lisr</button></a>
        </form:form>
</body>
</html>
