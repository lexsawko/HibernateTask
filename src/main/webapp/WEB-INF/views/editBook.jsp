<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>

<sql:query var="genres" dataSource="jdbc/TestDB">
    select * from genre
</sql:query>

<sql:query var="authors" dataSource="jdbc/TestDB">
    select * from author
</sql:query>


<html>
<head>
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
    <title>${method} book</title>
</head>
<body>
<h1>${method} book</h1>
        <form:form action="update_book" modelAttribute="book">
            <table class="margin">
                <c:if test="${method eq 'Edit'}">
                <th>ID</th>
                <td><form:input type="number" path="id" placeholder="ID" readonly="true"/></td>
                </c:if>
                <tr>
                    <th>Name</th>
                    <td><form:input type="text" path="name" placeholder="book_name" required="true"/></td>
                </tr>
                <tr>
                    <th>Pages</th>
                    <td><form:input type="number" path="pages" min="0" placeholder="pages" required="true"/></td>
                </tr>
                <tr>
                    <th>Age</th>
                    <td><form:input type="number" path="age" min="0" placeholder="age" required="true"/></td>
                </tr>
                <tr>
                    <th>Genre</th>
                    <td>
                        <form:select path="genre">
                            <c:forEach var="Genre" items="${genres.rows}">
                                <c:choose>
                                    <c:when test="${Genre.id == book.genre.id}">
                                        <form:option value="${Genre.id}" selected="true">${Genre.genre_name}</form:option>
                                    </c:when>
                                    <c:otherwise>
                                        <form:option value="${Genre.id}">${Genre.genre_name}</form:option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <th>Author</th>
                    <td>
                        <form:select path="author">
                            <c:forEach var="Author" items="${authors.rows}">
                                <c:choose>
                                    <c:when test="${Author.id == book.author.id}">
                                        <form:option value="${Author.id}" selected="true">${Author.first_name} ${Author.last_name}</form:option>
                                    </c:when>
                                    <c:otherwise>
                                        <form:option value="${Author.id}">${Author.first_name} ${Author.last_name}</form:option>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </form:select>
                    </td>
                </tr>
            </table>
            <button type="submit" class="btn-save">Save</button>
             <a href="/library"><button type="button" style="margin-left: 20px">Back to library</button></a>
            </form:form>
</body>
</html>
