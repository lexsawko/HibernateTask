<%@ taglib prefix="x" uri="http://localhost:8080" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script type="text/javascript" src="<c:url value="/resources/js/javascript.js"/>"></script>
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
    <title>Books</title>
</head>
<body>
<h1>Library</h1>
<table>
    <tr>
        <td class="td">
            <h2>Books</h2>
            <a href="edit_book"><button type="submit" class="btn-add">Add book</button></a>
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Pages</th>
                    <th>Age</th>
                    <th>Genre</th>
                    <th>Author</th>
                </tr>
                <c:forEach var="row" items="${books}">
                    <tr>
                        <td>
                                ${row.id}
                        </td>
                        <td>
                                ${row.name}
                        </td>
                        <td>
                                ${row.pages}
                        </td>
                        <td>
                                ${row.age}
                        </td>
                        <td>
                                ${row.genre.name}
                        </td>
                        <td>
                                ${row.author.firstName} ${row.author.lastName}
                        </td>
                        <td>
                            <a href="edit_book?id=${row.id}">
                                <button type="button" class="btn-edit">Edit</button></a>
                            <form method="post" action="delete_book?id=${row.id}">
                                <button type="submit" class="btn-delete">Delete</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </td>
        <td class="td">
            <h2>Authors</h2>
            <a href="edit_author"><button type="submit" class="btn-add">Add author</button></a>
                <table border="1">
                    <tr>
                        <th>ID</th>
                        <th>First name</th>
                        <th>Last name</th>
                    </tr>
                    <c:forEach var="author" items="${authors}">
                        <tr>
                            <td>${author.id}</td>
                            <td>${author.firstName}</td>
                            <td>${author.lastName}</td>
                            <td>
                                <a href="edit_author?id=${author.id}">
                                    <button type="button" class="btn-edit">Edit</button></a>
                                <form method="post" action="delete_author?id=${author.id}">
                                    <button type="submit" class="btn-delete">Delete</button>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
        </td>
        <td class="td">
            <h2>Genres</h2>
            <a href="edit_genre"><button type="submit" class="btn-add">Add genre</button></a>
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
                <c:forEach var="genre" items="${genres}">
                    <tr>
                        <td>${genre.id}</td>
                        <td>${genre.name}</td>
                        <td>
                            <a href="edit_genre?id=${genre.id}">
                                <button type="button" class="btn-edit">Edit</button></a>
                            <form method="post" action="delete_genre?id=${genre.id}">
                                <button type="submit" class="btn-delete">Delete</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </td>
    </tr>
</table>
<table>
    <tr>
        <td class="td">
            <p style="margin-top: 25px">Exception</p>
            <textarea rows="4" cols="50" id="exception"></textarea>
        </td>
        <td class="td">
            <a href="/users"><button type="button" class="button" >User list</button></a>
        </td>

    </tr>
</table>
</body>
</html>
